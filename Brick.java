import java.awt.*;

public class Brick {
    private int brickArray[][];
    private int brickWidth;
    private int brickHeight;
    private int row;
    private int column;
    private int blocksTotalWidth;
    private int brickPositionX;
    private int brickPositionY;
    private int totalBricks;

    public Brick(int row, int column) {
        this.brickWidth = 100;
        this.brickHeight = 40;
        this.row = row;
        this.column = column;
        this.brickArray = new int[row][column];
        this.initBricksInArray();
        this.blocksTotalWidth = (brickArray[0].length * this.brickWidth)/2;
        this.totalBricks = row*column;
    }

    public void setBrickPositionX(int brickPositionX) {
        this.brickPositionX = brickPositionX;
    }

    public void setBrickPositionY(int brickPositionY) {
        this.brickPositionY = brickPositionY;
    }

    public int getBrickPositionX() {
        return brickPositionX;
    }

    public int getBrickPositionY() {
        return brickPositionY;
    }

    public void setTotalBricks() {
        totalBricks-=1;
    }

    public int getTotalBricks(){
        return totalBricks;
    }

    public int[][] getBrickArray() {
        return brickArray;
    }

    public int getBrickWidth() {
        return brickWidth;
    }

    public int getBrickHeight() {
        return brickHeight;
    }

    public void setBricks(Graphics2D graphics2d) {
        for (int i=0; i<brickArray.length; i++) {
            for (int j=0; j<brickArray[i].length; j++) {
                if (brickArray[i][j] > 0){
                    graphics2d.setColor(Color.GREEN);
                    graphics2d.fillRect(j*brickWidth+50, i*brickHeight+50, brickWidth, brickHeight);
                    graphics2d.setStroke(new BasicStroke(8));
                    graphics2d.setColor(Color.BLACK);
                    graphics2d.drawRect(j*brickWidth+50, i*brickHeight + 50, brickWidth, brickHeight);
                }
            }
        }
    }

    public void initBricksInArray(){
        for (int i=0; i<brickArray.length; i++) {
            for (int j = 0; j < brickArray[i].length; j++) {
                brickArray[i][j] = 1;
            }
        }
    }
}
