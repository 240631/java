import java.awt.*;

public class Paddle {
    private int playerOrgPosX = 340;
    private int playerOrgPosY = 550;
    private int paddleOrgWidth = 100;
    private int playerPositionX;
    private int playerPositionY;
    private int paddleWidth;
    private int playerMove;
    private int paddleWidthTotal;
    private int paddleHeight;

    Paddle() {
        this.playerPositionX = playerOrgPosX;
        this.playerPositionY = playerOrgPosY;
        this.paddleWidth = paddleOrgWidth;
        this.playerMove = 10;
        this.paddleHeight = 5;
    }

    public int getPaddlePositionX() {
        return this.playerPositionX;
    }

    public int getPaddlePositionY() {
        return this.playerPositionY;
    }

    public int getPaddleWidth() {
        return this.paddleWidth;
    }

    public void setPaddleWidth() {
        paddleWidth -= 20;
        paddleWidthTotal += 20;
    }

    void moveRight() {
        if (playerPositionX >= 670 + paddleWidthTotal) {
            playerPositionX = 670 + paddleWidthTotal;
        } else {
            playerPositionX += playerMove;
        }
    }

    void moveLeft() {
        if (playerPositionX <= 10) {
            playerPositionX = 10;
        } else {
            playerPositionX -= playerMove;
        }
    }

    void drawPaddleColor(Graphics2D g2d, int failCounter) {
        switch(failCounter){
            case 0:
                g2d.setColor(Color.GREEN);
                g2d.fillRect(getPaddlePositionX(), getPaddlePositionY(), paddleWidth, paddleHeight);
                break;
            case 1:
                g2d.setColor(Color.YELLOW);
                g2d.fillRect(getPaddlePositionX(), getPaddlePositionY(), paddleWidth, paddleHeight);
                break;
            case 2:
                g2d.setColor(Color.RED);
                g2d.fillRect(getPaddlePositionX(), getPaddlePositionY(), paddleWidth, paddleHeight);
                break;
            default:
                g2d.setColor(Color.BLACK);
                g2d.fillRect(getPaddlePositionX(), getPaddlePositionY(), paddleWidth, paddleHeight);
                break;
        }
    }
}
