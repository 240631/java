import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Objects;

public class Game extends JPanel implements KeyListener, ActionListener {
    private boolean gameStatus = true;
    private boolean canShoot = false;
    private boolean startInfo = true;
    private int score = 0;
    private int failCounter;
    private int totalBricks = 21;
    private Timer time;
    private int delay = 6;
    private Paddle paddleObj;
    private Ball ballObj;
    private Brick brickObj;

    public Game() {
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        time = new Timer(delay, this);
        time.start();
        paddleObj = new Paddle();
        ballObj = new Ball();
        brickObj = new Brick(3, 7);
    }

    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        drawFrame(g2d);
        paddleObj.drawPaddleColor(g2d, failCounter);
        ballObj.drawBallColor(g2d, failCounter);
        brickObj.setBricks(g2d);
        drawScoreAndFails(g2d);
        if(!gameStatus){
            if(failCounter >= 3) {
                g2d.setColor(Color.RED);
                g2d.setFont(new Font("serif", Font.BOLD, 22));
                g2d.drawString("Game Over!", 350, 400);
                g2d.setColor(Color.BLUE);
                g2d.drawString("Bricks left: " + brickObj.getTotalBricks(), 345, 430);
                g2d.setColor(Color.GREEN);
                g2d.drawString("Scored: " + score, 365, 460);
            }
            if(brickObj.getTotalBricks() <= 0) {
                g2d.setColor(Color.GREEN);
                g2d.setFont(new Font("serif", Font.BOLD, 22));
                g2d.drawString("You won, bravo avocado!", 300, 400);
            }
        } else if (gameStatus) {
            if(startInfo) {
                if(!canShoot) {
                    g2d.setFont(new Font("serif", Font.BOLD, 22));
                    g2d.drawString("Press space to start.", 300, 400);
                } else if (canShoot) {
                    startInfo = false;
                    System.out.println(startInfo);
                }
            }
        }
        g2d.dispose();
    }

    void drawFrame(Graphics2D g2d){
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, 800, 600);   // x, y, width, height
        g2d.setColor(Color.RED);
        g2d.drawRect(0,0,783,560);
    }

    void drawScoreAndFails(Graphics2D g2d){
        g2d.setColor(Color.GREEN);
        g2d.setFont(new Font("serif", Font.BOLD, 22));
        g2d.drawString("Score: " + score, 50, 30);
        g2d.setColor(Color.BLUE);
        g2d.drawString("Bricks: " + brickObj.getTotalBricks(), 350, 30);
        g2d.setColor(Color.RED);
        g2d.drawString("Fails: " + failCounter, 650, 30);
    }

    void ballCollision() {
        if (ballObj.ballPositionX < 0) {
            ballObj.changeBallDirectionX();
        }
        if (ballObj.ballPositionX  >= 770) {
            ballObj.changeBallDirectionX();
        }
        if (ballObj.getBallPositionY() < 0) {
            ballObj.changeBallDirectionY();
        }
        if (ballObj.getBallPositionY() > 540) {
            failCounter += 1;
            canShoot = false;
            paddleObj.setPaddleWidth();
            ballObj = new Ball();
            System.out.println(failCounter);
        }
        Rectangle ballRect = new Rectangle(ballObj.ballPositionX, ballObj.getBallPositionY(), ballObj.getDiameter(), ballObj.getDiameter());
        Rectangle paddleRect = new Rectangle(paddleObj.getPaddlePositionX(), paddleObj.getPaddlePositionY(), paddleObj.getPaddleWidth(), 8);

        if(ballRect.intersects(paddleRect)) {
            ballObj.changeBallDirectionY();
        }
    }

    public void brickColision() {
        for(int i=0; i<brickObj.getBrickArray().length; i++) {
            for(int j=0; j<brickObj.getBrickArray()[0].length; j++) {
                if(brickObj.getBrickArray()[i][j] > 0) {
                    brickObj.setBrickPositionX(j * brickObj.getBrickWidth() + 50);
                    brickObj.setBrickPositionY(i * brickObj.getBrickHeight() + 50);
                    Rectangle brickRect = new Rectangle(brickObj.getBrickPositionX(), brickObj.getBrickPositionY(), brickObj.getBrickWidth(), brickObj.getBrickHeight());
                    Rectangle ballRect1 = new Rectangle(ballObj.ballPositionX, ballObj.getBallPositionY(), ballObj.getDiameter(), ballObj.getDiameter());
                    if(ballRect1.intersects(brickRect)){
                        brickObj.getBrickArray()[i][j] = 0;
                        score += 1;
                        brickObj.setTotalBricks();

                        System.out.println(brickRect.y+40);
                        System.out.println("ball y: " + ballObj.getBallPositionY());
                        System.out.println("ball y+5: " + (int)(ballObj.getBallPositionY()+5));
                        if (ballObj.getBallPositionY()+5 >= brickRect.y+brickRect.height || ballObj.getBallPositionY() <= brickRect.y) {
                            ballObj.changeBallDirectionY();
                            System.out.println("if");
                        } else {
                            ballObj.changeBallDirectionX();
                            System.out.println("else");
                        }
                        gameStatus = false;
                    }
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (gameStatus) {
            if (keyEvent.getKeyCode() == KeyEvent.VK_SPACE) {   // press space to start game
                canShoot = true;
                ballObj.setBallDirectionY();
            }
            if (keyEvent.getKeyCode() == KeyEvent.VK_RIGHT) {
                paddleObj.moveRight();
            }
            if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT) {
                paddleObj.moveLeft();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {  // tutaj cala akcja sie dzieje
        time.start();
        if (gameStatus) {
            if (canShoot) {
                ballObj.moveBall();
                ballCollision();
                brickColision();
                if(brickObj.getTotalBricks() <= 0) {
                    gameStatus = false;
                }
                if(failCounter >= 3){
                    gameStatus = false;
                }
            } else if(!canShoot) {
                ballObj.ballPositionX = paddleObj.getPaddlePositionX() + paddleObj.getPaddleWidth()/2-(ballObj.getDiameter()/2);
            }
        }
        repaint();
    }
}
