import java.awt.*;
import java.util.Random;

public class Ball {
    public int ballPositionX;
    private int ballPositionY;
    private int ballDirectionX;
    private int ballDirectionY;
    private int ballSpeed;
    private int[] randomNumbers;
    private Random rand = new Random();
    private int diameter;

    public Ball() {
        this.ballPositionX = 340;
        this.ballPositionY = 500;
        this.randomNumbers = new int[]{-1,1};
        this.ballDirectionX = randomNumbers[rand.nextInt(randomNumbers.length)];
        this.ballSpeed = 2;
        this.diameter = 20;
    }

    public int getBallPositionY() {
        return ballPositionY;
    }

    public void setBallDirectionY() {
        ballDirectionY = 1;
    }

    public int getDiameter() {
        return this.diameter;
    }

    void moveBall() {
        ballPositionX += ballSpeed*(-ballDirectionX);
        ballPositionY += ballSpeed*(-ballDirectionY);
    }

    void changeBallDirectionX() {
        ballDirectionX = -ballDirectionX;
    }

    void changeBallDirectionY() {
        ballDirectionY = -ballDirectionY;
    }

    void drawBallColor (Graphics2D g2d, int failCounter){
        g2d.setColor(Color.ORANGE);
        g2d.fillOval(ballPositionX, ballPositionY, diameter, diameter);
        if (failCounter >= 3) {
            g2d.setColor(Color.BLACK);
            g2d.fillOval(ballPositionX, ballPositionY, diameter, diameter);
        }
    }
}
